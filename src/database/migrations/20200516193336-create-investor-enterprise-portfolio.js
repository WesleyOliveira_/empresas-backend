'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('investor_enterprise_portfolio', { 
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      enterprise_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: "enterprise",
            key: "id"
        }
      },
      investor_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: "investor",
            key: "id"
        }
      },
    });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('investor_enterprise_portfolio');
  }
};
