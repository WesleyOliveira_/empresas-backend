'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('enterprise_type', 
  [
    {
        "id": 21,
        "enterprise_type_name": "Software"
    },
    {
        "id": 7,
        "enterprise_type_name": "Fashion"
    },
    {
        "id": 18,
        "enterprise_type_name": "Service"
    },
    {
        "id": 1,
        "enterprise_type_name": "Agro"
    },
    {
        "id": 11,
        "enterprise_type_name": "Health"
    },
    {
        "id": 14,
        "enterprise_type_name": "Media"
    },
    {
        "id": 24,
        "enterprise_type_name": "Transport"
    },
    {
        "id": 17,
        "enterprise_type_name": "Real Estate"
    },
    {
        "id": 4,
        "enterprise_type_name": "Eco"
    },
    {
        "id": 8,
        "enterprise_type_name": "Fintech"
    },
    {
        "id": 22,
        "enterprise_type_name": "Technology"
    },
    {
        "id": 3,
        "enterprise_type_name": "Biotech"
    },
    {
        "id": 19,
        "enterprise_type_name": "Smart City"
    },
    {
        "id": 9,
        "enterprise_type_name": "Food"
    },
    {
        "id": 6,
        "enterprise_type_name": "Education"
    },
    {
        "id": 23,
        "enterprise_type_name": "Tourism"
    },
    {
        "id": 5,
        "enterprise_type_name": "Ecommerce"
    },
    {
        "id": 15,
        "enterprise_type_name": "Mining"
    },
    {
        "id": 2,
        "enterprise_type_name": "Aviation"
    },
    {
        "id": 12,
        "enterprise_type_name": "IOT"
    },
    {
        "id": 20,
        "enterprise_type_name": "Social"
    },
    {
        "id": 13,
        "enterprise_type_name": "Logistics"
    },
    {
        "id": 10,
        "enterprise_type_name": "Games"
    },
    {
        "id": 16,
        "enterprise_type_name": "Products"
    }
  ], {}),

  down: (queryInterface) => queryInterface.bulkDelete('enterprise_type', null, {}),
};
