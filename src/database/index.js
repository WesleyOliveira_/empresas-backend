const Sequelize = require('sequelize');
const databaseConfig = require('../config/database')

const Enterprise_Types = require('../app/models/Enterprise_Types');
const Enterprise = require('../app/models/Enterprise');
const Investor = require('../app/models/Investor');
const Portfolio = require('../app/models/InvestorEnterprisePortfolio');

// Variável para receber todos os models
const models = [Enterprise_Types, Enterprise, Investor, Portfolio];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);
    
    // Conexão do banco com os models
    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }
}

module.exports = new Database();