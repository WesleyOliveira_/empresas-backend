const { Router } = require('express');
const { celebrate, Segments, Joi } = require('celebrate');

const EnterpriseController = require('./app/controllers/EnterpriseController');
const SessionController = require('./app/controllers/SessionController');
const authMiddleware = require('./app/middlewares/auth');

const routes = new Router();

routes.post('/api/v1/users/auth/sign_in', celebrate({
    [Segments.BODY]: Joi.object().keys({
        email: Joi.string().required().email(),
        password: Joi.string().required(),
    }),
}), SessionController.store);

routes.use(authMiddleware);

routes.get('/api/v1/enterprises', celebrate({
    [Segments.HEADERS]: Joi.object({
        'access-token': Joi.string().required(),
        client: Joi.string().required(),
        uid: Joi.string().required(),
    }).unknown(),
}), EnterpriseController.index);

routes.get('/api/v1/enterprises/:id', celebrate({
    [Segments.HEADERS]: Joi.object({
        'access-token': Joi.string().required(),
        client: Joi.string().required(),
        uid: Joi.string().required(),
    }).unknown(),
}), EnterpriseController.show);

 module.exports = routes;