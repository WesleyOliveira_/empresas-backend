require('dotenv/config');
const express = require('express');
const routes = require('./routes');

const Investor = require('./app/models/Investor');
const { errors } = require('celebrate');

var responseTime = require('response-time')

require('./database');

class App {
  constructor() {
    this.server = express();
    this.start();
    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.server.use(express.json());
    this.server.use(responseTime({header: 'X-Runtime'}));
  }
  
  routes() {
    this.server.use(routes);
    this.server.use(errors());
  }

  async start() {
    const investor =  await Investor.findOne({       
      where: { 
        email: 'testeapple@ioasys.com.br'
      },     
    });
    
    if(!investor) {
      await Investor.create(
        {
          "password": "12341234",
          "investor_name": "Teste Apple",
          "email": "testeapple@ioasys.com.br",
          "city": "BH",
          "country": "Brasil",
          "balance": 350000.0,
          "photo": "/uploads/investor/photo/1/cropped4991818370070749122.jpg",
          "portfolio_value": 350000.4,
          "first_access": false,
          "super_angel": false
        }
      );
    }
  }
}

module.exports = new App().server;