const jwt = require('jsonwebtoken');

const authConfig = require('../../config/authConfig');
const crypto = require('crypto');
const Cryptr = require('cryptr');

const Investor = require('../models/Investor');
const Enterprise = require('../models/Enterprise');
const Portfolio = require('../models/InvestorEnterprisePortfolio');

class SessionController {
    async store(req, res) {
        const { email, password } = req.body;

        //Verificando se existe
        const investor = await Investor.findOne({ where: { email } });
        if(!investor) {
            return res.status(401).json({ error: 'investor not exists' });
        }

        //Verificando se a senha não bate
        if(! (await investor.checkPass(password))) {
            return res.status(401).json({ error: 'Incorrect Password'});
        }

        const { id, investor_name, city, country,
            balance, photo, portfolio_value, first_access, super_angel } = investor;

        //Gerando token
        const token = jwt.sign({ id }, authConfig.secret, {
            expiresIn: authConfig.expiresIn,
        })

        //criando o objeto portfolio
        const portfolio = {
            enterprises_number: await Portfolio.count({
                where: { investor_id: id }
            }),
            enterprises: await Investor.findByPk(id, {
                include: {
                    model: Enterprise,
                    association: 'enterprises',
                    through: { attributes: []}
                }
            })
        }

        //Verificando se existem enterprises
        let enterprise = null
        if(portfolio.enterprises_number !== 0) {
            enterprise = portfolio.enterprises_number;
        }

        //Gerando X-Request-Id e criptografando o client
        const xRequestId = crypto.randomBytes(16).toString('HEX');

        const cryptr = new Cryptr(authConfig.cryptrPass);
        const client = await cryptr.encrypt(`${id}`);

        //Adicionando custom headers
        res.setHeader('access-token', token);
        res.setHeader('token-type', 'Bearer');
        res.setHeader('client', client);
        res.setHeader('expiry', authConfig.expiresIn);
        res.setHeader('uid', investor.email);
        res.setHeader('X-Request-Id', xRequestId);

        //Retorno da requisição
        return res.json({
            investor: {
                id,
                investor_name,
                email,
                city,
                country,
                balance,
                photo,
                portfolio: {
                    enterprises_number: portfolio.enterprises_number,
                    enterprises: portfolio.enterprises.enterprises
                },
                portfolio_value,
                first_access, 
                super_angel
            },
            enterprise,
            sucess: true
        });
    }
}

module.exports = new SessionController();