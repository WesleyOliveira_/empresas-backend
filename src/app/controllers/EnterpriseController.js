const Enterprise = require('../models/Enterprise');
const { Op } = require('sequelize');

class EnterpriseController {
  async index(req, res) {
    const { enterprise_types, name }  = req.query;

    if((!name) && (!enterprise_types))
    {
      const enterprise = await Enterprise.findAll({
        attributes: ['id', 'email_enterprise', 'facebook', 'twitter',
                     'linkedin', 'phone', 'own_enterprise', 'enterprise_name',
                     'photo', 'description', 'city', 'country', 'value', 'share_price'
        ],
        include: { 
          association: 'enterprise_type',
        }
      });
      return res.json({ enterprises: enterprise });
    }
    else {
      let where = {};      
      if(!name) {
        where = {
          enterprise_type_id: enterprise_types
        }
      }
      else if(!enterprise_types) {
        where = {
          enterprise_name: {
            [Op.iLike]: '%' + name + '%'
          },
        }
      }
      else {
        where = {
          enterprise_name: {
            [Op.iLike]: '%' + name + '%'
          },
          enterprise_type_id: enterprise_types
        }
      }

      const enterprise = await Enterprise.findAll({
         attributes: ['id', 'email_enterprise', 'facebook', 'twitter',
                      'linkedin', 'phone', 'own_enterprise', 'enterprise_name',
                      'photo', 'description', 'city', 'country', 'value', 'share_price'
         ],
         include: { 
           association: 'enterprise_type',
         },
         where: where
      });  

      return res.json({ enterprises: enterprise });
    }
  }
  
  async show (req, res) {
    const {id} = req.params;

    const enterprise = await Enterprise.findByPk(id, {
      attributes: ['id', 'email_enterprise', 'facebook', 'twitter',
                   'linkedin', 'phone', 'own_enterprise', 'enterprise_name',
                   'photo', 'description', 'city', 'country', 'value', 'share_price'
      ],
      include: { 
        association: 'enterprise_type',
      }
    });

    let sucess = true;
    if(!enterprise) {
      sucess = false;
    }

    return res.json({ enterprise, sucess });
  }
}

module.exports = new EnterpriseController();