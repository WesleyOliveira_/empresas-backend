const jwt = require('jsonwebtoken');
const { promisify } = require('util');
const authConfig = require('../../config/authConfig');
const Cryptr = require('cryptr');
const crypto = require('crypto');

const Investor = require('../models/Investor');

module.exports = async (req, res, next) => {
    const accesstoken = req.headers['access-token'];
    const { client, uid } = req.headers;

    //Verificando se o uid informado existe a partir do header uid
    const clientUidExist = await Investor.findOne({ where: { email: uid } });
    if(!clientUidExist) {
        return res.status(401).json({ error: 'Uid does not exist' });
    }

    //Verificando se o cliente informado existe a partir do header client
    const cryptr = new Cryptr(authConfig.cryptrPass);
    const clientValido = cryptr.decrypt(client);
    const clientExist = await Investor.findByPk(clientValido);
    if(!clientExist) {
        return res.status(401).json({ error: 'Client does not exist' });
    }

    //Verificando se o token foi realmente passado
    if(!accesstoken) {
        return res.status(401).json({ error: 'Token does not exist' });
    }

    try{
        const decoded = await promisify(jwt.verify)(accesstoken, authConfig.secret);

        const xRequestId = crypto.randomBytes(16).toString('HEX');

        //Adicionando custom headers
        res.setHeader('access-token', accesstoken);
        res.setHeader('token-type', 'Bearer');
        res.setHeader('client', client);
        res.setHeader('expiry', authConfig.expiresIn);
        res.setHeader('uid', uid);
        res.setHeader('X-Request-Id', xRequestId);

        return next();
    } catch (err) {
        return res.status(401).json({ error: 'Invalid Token' });
    }
};