const { Sequelize, Model } = require('sequelize');

class Portfolio extends Model {
    static init(sequelize) {
        super.init(
            {
                enterprise_id: Sequelize.INTEGER,
                investor_id: Sequelize.INTEGER
            },
            {
                sequelize,
                tableName: 'investor_enterprise_portfolio'
            }
        );
        return this;
    }
}

module.exports = Portfolio;

