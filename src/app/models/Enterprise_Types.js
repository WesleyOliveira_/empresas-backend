const { Sequelize, Model } = require('sequelize');

class Enterprise_Types extends Model {
    static init(sequelize) {
        super.init(
            {
                enterprise_type_name: Sequelize.STRING
            },
            {
                sequelize,
                tableName: 'enterprise_type'
            }
        );
        return this;
    }

    static associate (models) {
        this.hasMany(models.Enterprise, {
            foreignKey: 'enterprise_type_id', 
            as: 'enterprises'
        })
    };
}

module.exports = Enterprise_Types;