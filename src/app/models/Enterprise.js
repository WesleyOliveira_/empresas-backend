const { Sequelize, Model } = require('sequelize');

class Enterprise extends Model {
    static init(sequelize) {
        super.init(
            {
                email_enterprise: Sequelize.STRING,
                facebook: Sequelize.STRING,
                twitter: Sequelize.STRING,
                linkedin: Sequelize.STRING,
                phone: Sequelize.STRING,
                own_enterprise: Sequelize.BOOLEAN,
                enterprise_name: Sequelize.STRING,
                photo: Sequelize.STRING,
                description: Sequelize.STRING,
                city: Sequelize.STRING,
                country: Sequelize.STRING,
                value: Sequelize.INTEGER,
                share_price: Sequelize.DECIMAL
            },
            {
                sequelize,
                tableName: 'enterprise'
            }
        );
        return this;
    }

    static associate (models) {
        this.belongsTo(models.Enterprise_Types, {
            foreignKey: 'enterprise_type_id', 
            as: 'enterprise_type'
        }),
        this.belongsToMany(models.Investor, {
            foreignKey: 'enterprise_id', 
            through: 'investor_enterprise_portfolio',
            as: 'investors'
        })
    };
}

module.exports = Enterprise;