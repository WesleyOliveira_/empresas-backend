const { Sequelize, Model } = require('sequelize');
const bcrypt = require('bcryptjs');

class Investor extends Model {
    static init(sequelize) {
        super.init(
            {
                investor_name: Sequelize.STRING,
                password: Sequelize.STRING,
                email: Sequelize.STRING,
                city: Sequelize.STRING,
                country: Sequelize.STRING,
                balance: Sequelize.DECIMAL,
                photo: Sequelize.STRING,
                portfolio_value: Sequelize.DECIMAL,
                first_access: Sequelize.BOOLEAN,
                super_angel: Sequelize.BOOLEAN,
            },
            {
                sequelize,
                tableName: 'investor'
            }
        );

        this.addHook('beforeSave', async (investor) => {
            if(investor.password) {
                investor.password = await bcrypt.hash(investor.password, 8);
            }
        });
      return this;
    }
  
    checkPass(password) {
      return bcrypt.compare(password, this.password);
    }

    static associate (models) {
        this.belongsToMany(models.Enterprise, {
            foreignKey: 'investor_id', 
            through: 'investor_enterprise_portfolio',
            as: 'enterprises'
        })
    };
}

module.exports = Investor;